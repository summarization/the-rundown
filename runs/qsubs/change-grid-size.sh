#!/usr/bin/env bash
for f in singleproc_case*; do
  gs=`grep '#sensei_grid=' $f | sed -e 's/.*x//g'`
  hgs=$((gs/2))
  echo "$f ${gs} -> ${hgs}"
  mv $f /tmp
  sed -e "s/^sensei_grid=.*/sensei_grid=${hgs}x${hgs}x${hgs}/" /tmp/$f > $f
done
