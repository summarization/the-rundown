#!/usr/bin/env bash
old_end_time=1.0
new_end_time=0.08
for f in singleproc_case* ; do
  mv $f /tmp
  cat /tmp/$f | sed -e "s/--t-end ${old_end_time}/--t-end ${new_end_time}/g" > $f
done
