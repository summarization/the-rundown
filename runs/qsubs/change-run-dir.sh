#!/usr/bin/env bash
old_run_dir=ion/runs
new_run_dir=ion/runs_8_28
for f in singleproc_case* ; do
  mv $f /tmp
  cat /tmp/$f | sed -e "s,${old_run_dir},${new_run_dir},g" > $f
done
