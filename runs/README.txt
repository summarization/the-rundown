# Runs on Onyx
---------------

# -----------------------------------------------------------------------------
# Test cases
# -----------------------------------------------------------------------------

1) Base run for contours computations and image based compositing
- 3 contours
- 6x5 camera positions

2) VTKm contours and rendering then image based compositing
- 3 contours
- 6x5 camera positions

3) Base run for full volume dump

4) VTKm volume reduction and volume dump

# ------------------------------------------------------------------------------
# Runs
# ------------------------------------------------------------------------------

We want to test weak scaling across the various test cases with 100 timesteps

# nodes |    Grid size   |  Reduction 4 | Mode
===============================================
      1 | 1024x1024x1024 |  4 ( 262144) | 1024
      4 | 1625x1625x1625 |  4 (1047611) | 1625
      8 | 2048x2048x2048 |  5 ( 262144) | 2048
     16 | 2580x2580x2580 |  5 ( 524094) | 2580
     32 | 3250x3250x3250 |  5 (1047611) | 3250