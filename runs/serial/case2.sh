rundown_src=/p/home/acbauer/Summarization/the-rundown
rundown_build=/p/home/acbauer/Summarization/build

sensei_root=${rundown_src}/superbuild/sensei/src
sensei_ranks=1
sensei_grid=1024x1024x1024
mode=1024

oscillator=${rundown_build}/superbuild/sensei/build/bin/oscillator
config=${rundown_src}/runs/configs/case2-${mode}.xml
input=${rundown_src}/runs/inputs/periodic-${mode}.osc

${oscillator} -f ${config} ${input} -s ${sensei_grid} -t 0.01 --t-end 1.0 --shortlog
