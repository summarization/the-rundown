rundown_src=/Users/seb/Documents/code/vtk-m/rundown/the-rundown
rundown_build=/Users/seb/Documents/code/vtk-m/rundown/build

sensei_root=${rundown_src}/superbuild/sensei/src
sensei_ranks=1
sensei_grid=256x256x128

oscillator=${rundown_build}/superbuild/sensei/build/bin/oscillator
config=${rundown_src}/runs/configs/volume-extraction.xml
input=${rundown_src}/runs/inputs/complex-setup.osc

${oscillator} -f ${config} ${input} -s ${sensei_grid} -t 0.1 --t-end 15.0 --shortlog
