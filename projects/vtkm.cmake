set(vtkm_extra_cmake_args)

if (UNIX AND NOT APPLE)
  list (APPEND vtkm_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

superbuild_add_project(vtkm
  DEBUGGABLE
  DEPENDS
    # cxx11
  DEPENDS_OPTIONAL
    tbb
    #cuda
  CMAKE_ARGS
    -DVTKm_ENABLE_CUDA:BOOL=${rundown_ENABLE_CUDA}
    -DVTKm_ENABLE_TBB:BOOL=${rundown_ENABLE_TBB}
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DVTK_RENDERING_BACKEND:STRING=OpenGL2
    -DVTKm_ENABLE_TESTING:BOOL=OFF
    -DVTK_USE_SYSTEM_ZLIB:BOOL=${zlib_enabled}

    -DCMAKE_INSTALL_NAME_DIR:STRING=<INSTALL_DIR>/lib

    ${vtkm_extra_cmake_args}
)

superbuild_add_extra_cmake_args(
  -DVTKm_DIR:PATH=<INSTALL_DIR>/lib
)
