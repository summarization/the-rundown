set(tbb_extra_cmake_args)

if (UNIX AND NOT APPLE)
  list (APPEND tbb_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

superbuild_add_project(tbb
  DEBUGGABLE
  DEPENDS
    cxx11
  DEPENDS_OPTIONAL
    tbb cuda
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DVTK_RENDERING_BACKEND:STRING=OpenGL2

    -DVTK_USE_SYSTEM_ZLIB:BOOL=${zlib_enabled}

    -DCMAKE_INSTALL_NAME_DIR:STRING=<INSTALL_DIR>/lib

    ${tbb_extra_cmake_args})

superbuild_add_extra_cmake_args(
  -DVTKM_DIR:PATH=<INSTALL_DIR>/lib/cmake/tbb-${vtk_version}
)
