set(paraview_extra_cmake_args)
if (PV_NIGHTLY_SUFFIX)
  list(APPEND paraview_extra_cmake_args
    -DPV_NIGHTLY_SUFFIX:STRING=${PV_NIGHTLY_SUFFIX})
endif ()

set(paraview_disable_plugins
  EyeDomeLighting
  SciberQuestToolKit
  PointSprite
  NonOrthogonalSource
  PacMan
  StreamingParticles
  SierraPlotTools
  SLACTools
  UncertaintyRendering
  SurfaceLIC
  EyeDomeLighting
  RGBZView
  MobileRemoteControl)
foreach (plugin IN LISTS paraview_disable_plugins)
  list(APPEND paraview_extra_cmake_args
    -DPARAVIEW_BUILD_PLUGIN_${plugin}:BOOL=FALSE)
endforeach ()

if (APPLE)
  list(APPEND paraview_extra_cmake_args
    # We are having issues building mpi4py with Python 2.6 on Mac OSX. Hence,
    # disable it for now.
    -DPARAVIEW_USE_SYSTEM_MPI4PY:BOOL=ON)
endif ()

if (UNIX AND NOT APPLE)
  list (APPEND paraview_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

if (__BUILDBOT_INSTALL_LOCATION)
  list(APPEND paraview_extra_cmake_args
    -DPARAVIEW_CUSTOM_INSTALL_NAME_DIR:STRING=<INSTALL_DIR>
    -DPARAVIEW_DO_UNIX_STYLE_INSTALLS:BOOL=ON)
endif ()

list(APPEND paraview_extra_cmake_args
  -DPARAVIEW_EXTRA_EXTERNAL_PLUGINS:STRING=CMB_Plugin)

#this can't be quoted, since that will introduce an extra
#set of quotes into pqparaviewInitializer, and break the build
set(paraview_optional_plugins CMB_Plugin${_superbuild_list_separator}KMLExporter_Plugin)

find_package(EGL)
mark_as_advanced(CLEAR
  EGL_INCLUDE_DIR
  EGL_LIBRARY
  EGL_gldispatch_LIBRARY
  EGL_opengl_LIBRARY
)

superbuild_add_project(paraview
  DEBUGGABLE
  DEPENDS
    #boost
    #gdal
    png
    #python
    #qt
    vtkm
    zlib
    #netcdf
  DEPENDS_OPTIONAL
    # cxx11
    freetype hdf5 paraviewweb
    # qt4 qt5
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DPARAVIEW_BUILD_PLUGIN_CoProcessingScriptGenerator:BOOL=ON
    -DPARAVIEW_BUILD_QT_GUI:BOOL=OFF
    -DPARAVIEW_ENABLE_PYTHON:BOOL=${python_enabled}
    -DPARAVIEW_ENABLE_WEB:BOOL=OFF
    -DPARAVIEW_USE_MPI:BOOL=ON
    -DPARAVIEW_USE_VTKM:BOOL=ON
    -DVTK_USE_SYSTEM_HDF5:BOOL=${hdf5_enabled}
    -DVTK_OPENGL_HAS_EGL:BOOL=ON
    -DVTK_DEFAULT_RENDER_WINDOW_HEADLESS:BOOL=ON
    -DEGL_INCLUDE_DIR:PATH=${EGL_INCLUDE_DIR}
    -DEGL_LIBRARY:FILEPATH=${EGL_LIBRARY}
    -DEGL_gldispatch_LIBRARY:FILEPATH=${EGL_gldispatch_LIBRARY}
    -DEGL_opengl_LIBRARY:FILEPATH=${EGL_opengl_LIBRARY}
    -DHDF5_NO_FIND_PACKAGE_CONFIG_FILE:BOOL=ON
    -DVTK_USE_SYSTEM_NETCDF:BOOL=${netcdf_enabled}
    -DVTK_RENDERING_BACKEND:STRING=OpenGL2
    -DVTK_USE_X:BOOL=OFF
    -DVTK_DEFAULT_RENDER_WINDOW_OFFSCREEN:BOOL=ON
    -DPARAVIEW_ENABLE_WEB:BOOL=${paraviewweb_enabled}
    -DPARAVIEW_USE_ICE_T:BOOL=ON
    -DModule_vtkicet:BOOL=ON
    -DVTK_Group_MPI:BOOL=ON
    -DVTK_DISPATCH_AOS_ARRAYS:BOOL=ON
    #-DVTK_DISPATCH_SOA_ARRAYS:BOOL=ON
    #-DVTK_DISPATCH_TYPED_ARRAYS:BOOL=ON

    #currently catalyst is having problems on praxis so lets disable it for now
    -DPARAVIEW_ENABLE_CATALYST:BOOL=ON

    #-DModule_vtkRenderingMatplotlib:BOOL=ON
    -DModule_vtkAcceleratorsVTKm:BOOL=ON
    #-DModule_vtkAcceleratorsVTKmRendering:BOOL=ON
    -DModule_vtkRenderingGL2PSOpenGL2:BOOL=ON
    -DModule_vtkParallelMPI:BOOL=ON
    -DModule_vtkicet:BOOL=OFF

    # The Rundown needs to specify external plugins so that we can let paraview
    # properly install the plugins. So we sneakily force a variable that is an
    # implementation detail of paraview branding
    -DBPC_OPTIONAL_PLUGINS:INTERNAL=${paraview_optional_plugins}
    -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=TRUE
    # since VTK mangles all the following, I wonder if there's any point in
    # making it use system versions.
    -DVTK_USE_SYSTEM_FREETYPE:BOOL=${freetype_enabled}
#    currently png strips rpaths don't use this in cmb, so don't use this
#    -DVTK_USE_SYSTEM_PNG:BOOL=${png_enabled}
    -DVTK_USE_SYSTEM_ZLIB:BOOL=${zlib_enabled}

    # Specify the apple app install prefix. No harm in specifying it for all
    # platforms.
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications

    #If this is true paraview doesn't properly clean the paths to system
    #libraries like netcdf
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=TRUE
    -DCMAKE_MACOSX_RPATH:BOOL=FALSE

    # Ideally, we would keep up with the latest ParaView & VTK by avoiding the
    # use of legacy API. However, SENSEI uses vtkDataArrayTemplate.h.
    #-DVTK_LEGACY_REMOVE:BOOL=TRUE

    ${paraview_extra_cmake_args})

if (__BUILDBOT_INSTALL_LOCATION)
  set(paraview_paraview_dir <INSTALL_DIR>/lib/cmake/paraview-${paraview_version})
  set(paraview_vtk_dir ${paraview_paraview_dir})
else ()
  set(paraview_paraview_dir ${CMAKE_CURRENT_BINARY_DIR}/paraview/build)
  set(paraview_vtk_dir ${paraview_paraview_dir}/VTK)
endif ()

superbuild_add_extra_cmake_args(
  -DParaView_DIR:PATH=${paraview_paraview_dir}
  -DVTK_DIR:PATH=${paraview_vtk_dir}
)
