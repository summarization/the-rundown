set(sensei_extra_cmake_args)

if (UNIX AND NOT APPLE)
  list (APPEND sensei_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

superbuild_add_project(sensei
  DEBUGGABLE
  DEPENDS
    cxx11
    vtkm
    paraview
  DEPENDS_OPTIONAL
    tbb
    #cuda
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DENABLE_VTK_M:BOOL=ON
    -DENABLE_VTK_GENERIC_ARRAYS:BOOL=ON
    -DENABLE_SENSEI:BOOL=ON
    -DENABLE_PYTHON:BOOL=OFF
    -DENABLE_CATALYST:BOOL=ON
    -DENABLE_CATALYST_PYTHON:BOOL=OFF
    -DENABLE_CINEMA:BOOL=ON
    -DENABLE_OSCILLATORS:BOOL=ON
    -DENABLE_PARALLEL3D:BOOL=ON
    -DENABLE_PYTHON:BOOL=OFF
    -DENABLE_VTK_XMLP:BOOL=ON

    -DCMAKE_INSTALL_NAME_DIR:STRING=<INSTALL_DIR>/lib

    ${sensei_extra_cmake_args}
)

superbuild_add_extra_cmake_args(
  -DVTKM_DIR:PATH=<INSTALL_DIR>/lib/cmake/sensei-${vtk_version}
)
