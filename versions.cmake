# XXX: When updating this, update the version number in CMakeLists.txt as well.
# The current version of ParaView is post 5.4.0 RC2

set(paraview_repo "https://gitlab.kitware.com/dcthomp/paraview.git")
set(paraview_revision "origin/perverse-vtkm-branch")

# set(sensei_repo "https://gitlab.kitware.com/sensei/sensei.git")
# set(sensei_revision "origin/cinema-catalyst")

# set(vtkm_repo "https://gitlab.kitware.com/vtk/vtk-m.git")
# set(vtkm_revision "origin/master")

set(sensei_repo "https://gitlab.kitware.com/sensei/sensei.git")
set(sensei_revision "origin/cinema-vtkm-wip")

set(vtkm_repo "https://gitlab.kitware.com/dcthomp/vtk-m.git")
set(vtkm_revision "origin/stolen-arrays-neighborhood-work")

if (USE_PARAVIEW_master)
  set(paraview_revision origin/master)
endif ()
superbuild_set_revision(paraview
  GIT_REPOSITORY "${paraview_repo}"
  GIT_TAG        "${paraview_revision}")

superbuild_set_revision(sensei
  GIT_REPOSITORY "${sensei_repo}"
  GIT_TAG        "${sensei_revision}"
)

superbuild_set_revision(vtkm
  GIT_REPOSITORY ${vtkm_repo}
  GIT_TAG        ${vtkm_revision}
)

superbuild_set_revision(tbb
  GIT_REPOSITORY  "https://github.com/01org/tbb.git"
  GIT_TAG         origin/tbb_2017
)
