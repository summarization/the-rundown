#! /usr/bin/env node

/* eslint-disable */

var fs = require('fs');
var path = require('path');
var program = require('commander');
var shell = require('shelljs');

program
  .version('1.0.0')
  .option('-d, --data [directory/http]', 'Data directory to process')
  .option('-v, --volume [32x32x32]', 'Comple volume size')
  .parse(process.argv);

if (!process.argv.slice(2).length || !program.help || !program.data || !program.volume) {
    program.outputHelp();
    process.exit();
}

var volumeSplits = null;
var volumeSizes = program.volume.split('x').map(Number);
var pieceSizes = null;

console.log('volumeSizes', volumeSizes);

// Filter directory to process
function filterDataDir(file) {
    return file.match(/volume.json$/);
}

function extractVolumeSplit(dirPath) {
    var metaFile = require(path.join(dirPath, 'volume.json'));
    pieceSizes = [
        (metaFile.extent[1] - metaFile.extent[0] + 1),
        (metaFile.extent[3] - metaFile.extent[2] + 1),
        (metaFile.extent[5] - metaFile.extent[4] + 1),
    ]
    volumeSplits = [
        volumeSizes[0] / pieceSizes[0],
        volumeSizes[1] / pieceSizes[1],
        volumeSizes[2] / pieceSizes[2],
    ];
    volumeSplits.push(volumeSplits[0] * volumeSplits[1] * volumeSplits[2]);

    console.log('volumeSplits', volumeSplits);
    console.log('pieceSizes', pieceSizes);

}

function fileToDataArray(filePath) {
    var fd = fs.openSync(filePath, 'r');
    var length = fs.fstatSync(fd).size;
    var buffer = Buffer.alloc(length);
    fs.readSync(fd, buffer, 0, length, 0)
    fs.closeSync(fd);
    return new Float32Array(buffer.buffer, 0, buffer.byteLength/4);
}

function dataArrayToFile(dataArray, filePath) {
    var wstream = fs.createWriteStream(filePath);


    var buffer = new Buffer(dataArray.length*4);
    for(var i = 0; i < dataArray.length; i++){
        buffer.writeFloatLE(dataArray[i], i*4);
    }

    wstream.write(buffer);
    wstream.end();
}

function processDataDirectory(dirPath) {
    if (!volumeSplits) {
        extractVolumeSplit(dirPath);
    }

    console.log('process', dirPath);

    // Load each volume piece
    var volumes = [];
    var nbVolumes = volumeSplits[3];
    for (var volumeIdx = 0; volumeIdx < nbVolumes; volumeIdx++) {
        volumes.push(fileToDataArray(path.join(dirPath, volumeIdx ? `volume_${volumeIdx}.data`: 'volume.data')));
    }

    // Create and fill reconstructed volume
    var resultDataArray = new Float32Array(volumeSizes[0] * volumeSizes[1] * volumeSizes[2]);
    var volumeOffsets = [0, 0, 0];
    for (var k = 0; k < volumeSizes[2]; k++) {
        volumeOffsets[2] = Math.floor(volumeSplits[2] * k / volumeSizes[2]) * volumeSplits[1] * volumeSplits[0];
        for (var j = 0; j < volumeSizes[1]; j++) {
            volumeOffsets[1] = Math.floor(volumeSplits[1] * j / volumeSizes[1]) * volumeSplits[0];
            for (var i = 0; i < volumeSizes[0]; i++) {
                volumeOffsets[0] = Math.floor(volumeSplits[0] * i / volumeSizes[0]);
                var globalIdx = i + (j * volumeSizes[0]) + (k * volumeSizes[0] * volumeSizes[1]);
                var pieceIdx = (i % pieceSizes[0]) + ((j % pieceSizes[1]) * pieceSizes[0]) + ((k % pieceSizes[2]) * pieceSizes[0] * pieceSizes[1]);
                var piece = volumes[volumeOffsets[0] + volumeOffsets[1] + volumeOffsets[2]];
                resultDataArray[globalIdx] = piece[pieceIdx];
                // console.log(`${i}, ${j}, ${k} => ${globalIdx}:${pieceIdx}(${volumeOffsets[0] + volumeOffsets[1] + volumeOffsets[2]}) value: ${piece[pieceIdx]}`);
            }
        }
    }

    // Write data
    dataArrayToFile(resultDataArray, path.join(dirPath, 'merged_volume.data'));

    // Write metadata
    var metaFile = require(path.join(dirPath, 'volume.json'));
    metaFile.extent = [
        0, volumeSizes[0] - 1,
        0, volumeSizes[1] - 1,
        0, volumeSizes[2] - 1,
    ];
    metaFile.pointData.arrays[0].data.size = volumeSizes[0] * volumeSizes[1] * volumeSizes[2];
    metaFile.pointData.arrays[0].data.ref.id = 'merged_volume.data';
    fs.writeFileSync(path.join(dirPath, 'merged_volume.json'), JSON.stringify(metaFile));
}

var fileToProcess = shell
    .find(program.data)
    .filter(filterDataDir)
    .map(path.dirname);

// processDataDirectory(fileToProcess[0]);
fileToProcess.forEach(processDataDirectory);
