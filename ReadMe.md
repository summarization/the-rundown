<p style="text-align:center">
  <img src="images/TheRundown.png" alt="The Rundown SuperBuild Logo" align="middle" style="width: 200px;"/>
</p>

# The Rundown

The Rundown is a collection of software already in progress, just configured
to make the development summarization features that run in parallel as simple as possible.
To help make the process of building these libraries and applications, we have created a
superbuild.

# Requirements

* CMake version 3.6 or greater with SSL support (Binaries from `cmake.org`
  have this already; custom built CMake binaries need to use
  `CMAKE_USE_SYSTEM_CURL=ON`).
* ninja or make - (the Windows build requires ninja)
* Checkout of [The Rundown superbuild git repo](https://gitlab.kitware.com/summarization/the-rundown)
* C++ Compiler
 * XCode 7.1 or greater
 * GCC 4.8 or higher
 * Visual Studio 2013 64 bit
* Ubuntu 16.04 specific
 * sudo apt-get install m4
 * sudo apt-get install build-essential
 * sudo apt-get install libxt-dev

Note that the build process will also download additional tarballs and
checkout additional git repos so you will also need an internet connection.

# Building the rundown using the superbuild process

## Prepping the Git Repo

1. Clone the CMB SuperBuild Repo using `git clone https://gitlab.kitware.com/summarization/the-rundown.git`
2. Using a shell in the cloned repository, run `git submodule update --init`

## Configuring the Build Using CMake

There are two possible methods you can use: CMake GUI or the ccmake command line tool

### Using the CMake GUI

![](images/CmakeScreenShot.png)

1. Select the source directory that contains the cmake superbuild git repo.
2. Select the build directory to be used to hold the build.  Note that due to
   a bug in git this should not be a subdirectory of the git source repository.

### Using ccmake commandline tool

1. Make the directory you want to build in
2. cd into that directory
3. If you are building with ninja (as oppose to make) run
   `ccmake -G Ninja PathToYourSuperBuildRepo`, else omit the `-G Ninja`

### Configuring the superbuild

* By default the build will be in Release Mode (not Debug) - this can be
  changed using the `CMAKE_BUILD_TYPE_cmb` variable. Similar variables exist
  for other projects depending on the setup including `paraview`.
* Tell CMake to configure
* Tell CMake to generate

## Building The Rundown's superbuild

* cd into the build directory
* run make or ninja - depending on which build system you previously selected.

## Building an installable package

* cd into the build directory
* run ctest -R cpack
